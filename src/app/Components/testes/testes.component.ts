import { Component, OnInit } from '@angular/core';
import { ElasticsearchService } from 'src/app/Services/elasticsearch-service.service';

@Component({
  selector: 'app-testes',
  templateUrl: './testes.component.html',
  styleUrls: ['./testes.component.css']
})
export class TestesComponent implements OnInit {

  constructor(private es: ElasticsearchService) { }

  ngOnInit(): void {
    this.es.getAllDocumentsWithScroll("commune").subscribe(result => {
      console.log(result);
    },
    error => {
      console.log(error)
    }
    );
  }

}
