import { Injectable } from '@angular/core';

import { Client, SearchResponse } from 'elasticsearch';
import { from, Observable } from 'rxjs';

@Injectable()
export class ElasticsearchService {

  private client: Client;

  constructor() {
    this.client = new Client({
      host: 'http://10.9.6.222/es/'
    });
  }

  createIndex(name: any): any {
    return this.client.indices.create(name);
  }

  isAvailable(): any {
    return this.client.ping({
      requestTimeout: Infinity,
      body: 'hello JavaSampleApproach!'
    });
  }

  addToIndex(value: any): any {
    return this.client.create(value);
  }

  getAllDocumentsWithScroll<T>(_index: string, _type: string = "doc", _size: string = "1000"): Observable<SearchResponse<T>> {
    return from(this.client.search<T>({
      index: _index,
      type: _type,
      scroll: '1m',
      filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      body: {
        'size': _size,
        'query': {
          'match_all': {}
        },
        'sort': [
          { '_uid': { 'order': 'asc' } }
        ]
      }
    }));
  }

  getNextPage(scroll_id: any): any {
    return this.client.scroll({
      scrollId: scroll_id,
      scroll: '1m',
      filterPath: ['hits.hits._source', 'hits.total', '_scroll_id']
    });
  }

  fullTextSearch(_index: string, _type: string, _field: string, _queryText: string): any {
    return this.client.search({
      index: _index,
      type: _type,
      filterPath: ['hits.hits._source', 'hits.total', '_scroll_id'],
      body: {
        'query': {
          'match_phrase_prefix': {
            [_field]: _queryText,
          }
        }
      },
      '_source': ['fullname', 'address']
    });
  }
}
